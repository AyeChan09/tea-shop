-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for tea_shop
CREATE DATABASE IF NOT EXISTS `tea_shop` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tea_shop`;

-- Dumping structure for table tea_shop.acc_admin_menu_item
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `is_show` tinyint(4) DEFAULT NULL,
  `path` varchar(255) NOT NULL DEFAULT '',
  `external_href` varchar(255) DEFAULT NULL,
  `external_href_target` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `class` varchar(255) NOT NULL DEFAULT '',
  `order_id` int(11) DEFAULT NULL,
  `hasChild` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.acc_admin_menu_item: ~9 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item` (`id`, `title`, `is_show`, `path`, `external_href`, `external_href_target`, `icon`, `class`, `order_id`, `hasChild`, `parent_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'Menu', 1, '', NULL, NULL, 'fa fa-cubes', '', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Menu List', 1, '/synapse/product/product_list', NULL, NULL, '', '', 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'POS', 1, '/synapse/pos/pos', NULL, NULL, 'fa fa-calculator', '', 5, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Expense', 1, '', NULL, NULL, 'fas fa-dollar-sign', '', 2, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Expense List', 1, '/synapse/expense/expense_list', NULL, NULL, '', '', 1, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'Report', 1, '', NULL, NULL, 'fa fa-list', '', 3, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'Sale', 1, '/synapse/report/sale', NULL, NULL, '', '', 1, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'User List', 1, '/synapse/user/user_list', NULL, NULL, '', '', 1, 0, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'User', 1, '', NULL, NULL, 'fa fa-users', '', 4, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, ' Sale Detail', 1, '/synapse/report/sale_detail', NULL, NULL, '', '', 2, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item` ENABLE KEYS */;

-- Dumping structure for table tea_shop.acc_admin_menu_item_option
CREATE TABLE IF NOT EXISTS `acc_admin_menu_item_option` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `acc_admin_menu_item_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.acc_admin_menu_item_option: ~9 rows (approximately)
/*!40000 ALTER TABLE `acc_admin_menu_item_option` DISABLE KEYS */;
INSERT INTO `acc_admin_menu_item_option` (`id`, `acc_admin_menu_item_id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 2, 'allow_add', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 2, 'allow_edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 2, 'allow_delete', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 5, 'allow_add', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 5, 'allow_edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 5, 'allow_delete', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 5, 'allow_view', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 8, 'allow_add', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 8, 'allow_edit', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 8, 'allow_delete', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 8, 'allow_view', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_admin_menu_item_option` ENABLE KEYS */;

-- Dumping structure for table tea_shop.acc_list_setting
CREATE TABLE IF NOT EXISTS `acc_list_setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT '',
  `field_name` varchar(255) DEFAULT NULL,
  `type` enum('string','currency','date') DEFAULT NULL,
  `is_show` tinyint(4) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.acc_list_setting: ~9 rows (approximately)
/*!40000 ALTER TABLE `acc_list_setting` DISABLE KEYS */;
INSERT INTO `acc_list_setting` (`id`, `list_name`, `label`, `field_name`, `type`, `is_show`, `order_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'product', 'name', 'name', 'string', 1, 1, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'product', 'price', 'price', 'string', 1, 2, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'expense', 'expense_no', 'expense_no', 'string', 1, 1, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'expense', 'total', 'total', 'currency', 1, 2, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'expense', 'remark', 'remark', 'string', 1, 3, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'user', 'username', 'username', 'string', 1, 1, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 'user', 'email', 'email', 'string', 1, 2, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 'user', 'mobile', 'mobile', 'string', 1, 3, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 'user', 'gender', 'gender', 'string', 1, 4, '2018-07-28 12:48:43', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `acc_list_setting` ENABLE KEYS */;

-- Dumping structure for table tea_shop.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.category: ~0 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table tea_shop.company_running_number_setting
CREATE TABLE IF NOT EXISTS `company_running_number_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usage` varchar(255) DEFAULT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `last_number` int(11) DEFAULT NULL,
  `length` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.company_running_number_setting: ~2 rows (approximately)
/*!40000 ALTER TABLE `company_running_number_setting` DISABLE KEYS */;
INSERT INTO `company_running_number_setting` (`id`, `usage`, `prefix`, `format`, `last_number`, `length`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'product', 'S', 'Ycx', 1, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'expense', 'S', 'EpYmdx', 1, '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `company_running_number_setting` ENABLE KEYS */;

-- Dumping structure for table tea_shop.expense
CREATE TABLE IF NOT EXISTS `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_no` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.expense: ~6 rows (approximately)
/*!40000 ALTER TABLE `expense` DISABLE KEYS */;
INSERT INTO `expense` (`id`, `expense_no`, `remark`, `total`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 'Ep201808120001', NULL, 16100, '2018-07-29 15:31:52', NULL, '2018-08-12 15:31:52', NULL, NULL, NULL),
	(2, 'Ep201808120002', NULL, 3200, '2018-07-30 15:34:04', NULL, '2018-08-12 15:34:04', NULL, NULL, NULL),
	(5, 'Ep201808120003', NULL, 4800, '2018-08-07 16:33:09', NULL, '2018-08-12 16:33:09', NULL, NULL, NULL),
	(6, 'Ep201808120004', NULL, 6400, '2018-08-07 16:36:23', NULL, '2018-08-12 16:36:23', NULL, NULL, NULL),
	(7, 'Ep201809130001', NULL, 1650, '2018-09-12 16:24:02', NULL, '2018-09-13 16:24:02', NULL, NULL, NULL),
	(8, 'Ep201809130002', NULL, 9050, '2018-09-13 16:24:48', NULL, '2018-09-13 16:24:48', NULL, NULL, NULL),
	(9, 'Ep201809180001', NULL, 454455, '2018-09-17 19:09:56', NULL, '2018-09-18 19:09:56', NULL, NULL, NULL);
/*!40000 ALTER TABLE `expense` ENABLE KEYS */;

-- Dumping structure for table tea_shop.expense_item
CREATE TABLE IF NOT EXISTS `expense_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_id` int(11) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.expense_item: ~12 rows (approximately)
/*!40000 ALTER TABLE `expense_item` DISABLE KEYS */;
INSERT INTO `expense_item` (`id`, `expense_id`, `item_name`, `price`, `qty`, `total`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 'milk oil', 800, 10, 8000, '2018-08-12 15:31:52', NULL, '2018-08-12 15:31:52', NULL, NULL, NULL),
	(2, 1, 'chicken', 1200, 3, 3600, '2018-08-12 15:31:52', NULL, '2018-08-12 15:31:52', NULL, NULL, NULL),
	(3, 1, 'pork', 1000, 2, 2000, '2018-08-12 15:31:52', NULL, '2018-08-12 15:31:52', NULL, NULL, NULL),
	(4, 1, 'sugar', 500, 5, 2500, '2018-08-12 15:31:53', NULL, '2018-08-12 15:31:53', NULL, NULL, NULL),
	(5, 2, 'sugar', 300, 4, 1200, '2018-08-12 15:34:04', NULL, '2018-08-12 15:34:04', NULL, NULL, NULL),
	(6, 2, 'vegetables', 1000, 2, 2000, '2018-08-12 15:34:05', NULL, '2018-08-12 15:34:05', NULL, NULL, NULL),
	(9, 5, 'milk oil', 800, 6, 4800, '2018-08-12 16:33:09', NULL, '2018-08-12 16:33:09', NULL, NULL, NULL),
	(10, 6, 'နိုါဆီ', 800, 5, 4000, '2018-08-12 16:36:23', NULL, '2018-08-12 16:36:23', NULL, NULL, NULL),
	(11, 6, 'ျကက္သား', 1200, 2, 2400, '2018-08-12 16:36:23', NULL, '2018-08-12 16:36:23', NULL, NULL, NULL),
	(12, 7, 'Gurbage', 900, 1, 900, '2018-09-13 16:24:02', NULL, '2018-09-13 16:24:02', NULL, NULL, NULL),
	(13, 7, 'Lime', 50, 10, 500, '2018-09-13 16:24:02', NULL, '2018-09-13 16:24:02', NULL, NULL, NULL),
	(14, 7, 'Spicy', 250, 1, 250, '2018-09-13 16:24:02', NULL, '2018-09-13 16:24:02', NULL, NULL, NULL),
	(15, 8, 'chick', 1250, 5, 6250, '2018-09-13 16:24:48', NULL, '2018-09-13 16:24:48', NULL, NULL, NULL),
	(16, 8, 'pork', 1400, 2, 2800, '2018-09-13 16:24:48', NULL, '2018-09-13 16:24:48', NULL, NULL, NULL),
	(17, 9, 'jfeijf', 333, 3, 999, '2018-09-18 19:09:56', NULL, '2018-09-18 19:09:56', NULL, NULL, NULL),
	(18, 9, 'ffef', 6768, 67, 453456, '2018-09-18 19:09:56', NULL, '2018-09-18 19:09:56', NULL, NULL, NULL);
/*!40000 ALTER TABLE `expense_item` ENABLE KEYS */;

-- Dumping structure for table tea_shop.lang_list
CREATE TABLE IF NOT EXISTS `lang_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `abbrev` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.lang_list: ~5 rows (approximately)
/*!40000 ALTER TABLE `lang_list` DISABLE KEYS */;
INSERT INTO `lang_list` (`id`, `name`, `abbrev`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'English', 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'Chinese', 'zh', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'Indonesia', 'ind', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 'Malaysia', 'my', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 'Myanmar', 'mm', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_list` ENABLE KEYS */;

-- Dumping structure for table tea_shop.lang_presentation
CREATE TABLE IF NOT EXISTS `lang_presentation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template` varchar(255) DEFAULT NULL,
  `en` varchar(255) DEFAULT NULL,
  `zh` varchar(255) DEFAULT NULL,
  `ind` varchar(255) DEFAULT NULL,
  `vi` varchar(255) DEFAULT NULL,
  `mm` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.lang_presentation: ~19 rows (approximately)
/*!40000 ALTER TABLE `lang_presentation` DISABLE KEYS */;
INSERT INTO `lang_presentation` (`id`, `template`, `en`, `zh`, `ind`, `vi`, `mm`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 'login', 'Login', 'Login', 'Login', 'Login', 'Login', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 'number', 'Number', 'Number', 'Number', 'Number', 'Number', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'search', 'Search', 'Search', 'Search', 'Search', 'Search', '2018-01-03 14:47:42', 'guest', '2018-01-03 14:47:42', NULL, NULL, NULL, NULL),
	(4, 'filter_name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', 'Filter Name', '2018-01-03 14:47:42', 'guest', '2018-01-03 14:47:42', NULL, NULL, NULL, NULL),
	(5, 'active_filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', 'Active Filter', '2018-01-03 14:47:42', 'guest', '2018-01-03 14:47:42', NULL, NULL, NULL, NULL),
	(6, 'user_group', 'User Group', 'User Group', 'User Group', 'User Group', 'User Group', '2018-01-03 14:47:42', 'guest', '2018-01-03 14:47:42', NULL, NULL, NULL, NULL),
	(7, 'from_Date', 'From Date', 'From Date', 'From Date', 'From Date', 'From Date', '2018-01-03 14:47:43', 'guest', '2018-01-03 14:47:43', NULL, NULL, NULL, NULL),
	(8, 'to_date', 'To Date', 'To Date', 'To Date', 'To Date', 'To Date', '2018-01-03 14:47:43', 'guest', '2018-01-03 14:47:43', NULL, NULL, NULL, NULL),
	(9, 'saved_search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', 'Saved Search', '2018-01-03 14:47:43', 'guest', '2018-01-03 14:47:43', NULL, NULL, NULL, NULL),
	(10, 'sort_by', 'Sort By', 'Sort By', 'Sort By', 'Sort By', 'Sort By', '2018-01-03 14:47:44', 'guest', '2018-01-03 14:47:44', NULL, NULL, NULL, NULL),
	(11, 'sort_dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', 'Sort Dir', '2018-01-03 14:47:44', 'guest', '2018-01-03 14:47:44', NULL, NULL, NULL, NULL),
	(12, 'ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', 'Ascending', '2018-01-03 14:47:44', 'guest', '2018-01-03 14:47:44', NULL, NULL, NULL, NULL),
	(13, 'descending', 'Descending', 'Descending', 'Descending', 'Descending', 'Descending', '2018-01-03 14:47:44', 'guest', '2018-01-03 14:47:44', NULL, NULL, NULL, NULL),
	(14, 'action', 'Action', 'Action', 'Action', 'Action', 'Action', '2018-01-03 14:47:44', 'guest', '2018-01-03 14:47:44', NULL, NULL, NULL, NULL),
	(15, 'clear', 'Clear', 'Clear', 'Clear', 'Clear', 'Clear', '2018-01-03 14:47:44', 'guest', '2018-01-03 14:47:44', NULL, NULL, NULL, NULL),
	(16, 'save', 'Save', 'Save', 'Save', 'Save', 'Save', '2018-01-03 14:47:45', 'guest', '2018-01-03 14:47:45', NULL, NULL, NULL, NULL),
	(17, 'delete', 'Delete', 'Delete', 'Delete', 'Delete', 'Delete', '2018-01-03 14:47:45', 'guest', '2018-01-03 14:47:45', NULL, NULL, NULL, NULL),
	(18, 'Product', 'Product', 'Product', 'Product', 'Product', 'Product', '2018-01-03 14:47:45', 'guest', '2018-01-03 14:47:45', NULL, NULL, NULL, NULL),
	(19, 'Product List', 'Product List', 'Product List', 'Product List', 'Product List', 'Product List', '2018-01-03 14:47:45', 'guest', '2018-01-03 14:47:45', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `lang_presentation` ENABLE KEYS */;

-- Dumping structure for table tea_shop.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tea_shop.migrations: ~2 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table tea_shop.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tea_shop.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table tea_shop.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.product: ~13 rows (approximately)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `category_id`, `name`, `price`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, NULL, 'tea(sweet)', 400, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, NULL, 'tea(rich intaste)', 400, '2018-07-28 04:46:57', 'guest', '2018-07-28 04:46:57', NULL, NULL, NULL),
	(3, NULL, 'fried chicken', 1200, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(4, NULL, 'milk', 500, '2018-07-28 04:46:57', 'guest', '2018-07-28 04:46:57', NULL, NULL, NULL),
	(5, NULL, 'fried rice(American)', 2000, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(6, NULL, 'fried rice(PaShuu)', 2000, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(7, NULL, 'fried rice(Singapore)', 2000, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(8, NULL, 'fried noodle', 2000, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(9, NULL, 'Milo', 500, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(10, NULL, 'Coffee', 300, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(11, NULL, 'Cold Drink(Max)', 450, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(12, NULL, 'Dim Sum', 800, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(13, NULL, 'Ramen (Spicy)', 4300, '2018-08-12 15:36:26', 'guest', '2018-08-12 16:22:27', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Dumping structure for table tea_shop.slip
CREATE TABLE IF NOT EXISTS `slip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` int(11) DEFAULT NULL,
  `grand_total` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.slip: ~12 rows (approximately)
/*!40000 ALTER TABLE `slip` DISABLE KEYS */;
INSERT INTO `slip` (`id`, `total`, `grand_total`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 8400, NULL, '2018-07-29 08:51:30', 'guest', '2018-07-29 08:51:30', NULL, NULL, NULL),
	(2, 4100, NULL, '2018-07-29 09:04:42', 'guest', '2018-07-29 09:04:42', NULL, NULL, NULL),
	(3, 15200, NULL, '2018-07-29 12:39:39', 'guest', '2018-07-30 12:39:39', NULL, NULL, NULL),
	(4, 1200, NULL, '2018-07-29 12:40:15', 'guest', '2018-08-01 12:40:15', NULL, NULL, NULL),
	(5, 4000, NULL, '2018-08-07 17:09:01', 'guest', '2018-08-07 17:09:01', NULL, NULL, NULL),
	(10, 4500, NULL, '2018-08-07 17:26:10', 'guest', '2018-08-07 17:26:10', NULL, NULL, NULL),
	(11, 5300, NULL, '2018-08-07 17:27:08', 'guest', '2018-08-08 17:27:08', NULL, NULL, NULL),
	(12, 7900, NULL, '2018-08-12 16:38:19', 'guest', '2018-08-12 16:38:19', NULL, NULL, NULL),
	(13, 6800, NULL, '2018-09-10 15:53:09', 'guest', '2018-09-13 15:53:09', NULL, NULL, NULL),
	(14, 1400, NULL, '2018-09-13 15:53:23', 'guest', '2018-09-14 15:53:23', NULL, NULL, NULL),
	(15, 10800, NULL, '2018-09-14 15:53:44', 'guest', '2018-09-15 15:53:44', NULL, NULL, NULL),
	(16, 10800, NULL, '2018-09-15 15:53:44', 'guest', '2018-09-15 15:53:44', NULL, NULL, NULL),
	(17, 4000, 4200, '2018-09-16 15:47:22', 'guest', '2018-09-16 15:47:22', NULL, NULL, NULL),
	(18, 6800, 7140, '2018-09-16 15:49:47', 'guest', '2018-09-16 15:49:47', NULL, NULL, NULL),
	(19, 8300, 8715, '2018-09-18 18:49:13', 'guest', '2018-09-18 18:49:13', NULL, NULL, NULL),
	(20, 10000, 10500, '2018-09-18 18:50:56', 'guest', '2018-09-18 18:50:56', NULL, NULL, NULL),
	(21, 14600, 15330, '2018-10-01 18:24:32', 'guest', '2018-10-01 18:24:32', NULL, NULL, NULL),
	(22, 9200, 9660, '2018-10-01 18:43:20', 'guest', '2018-10-01 18:43:20', NULL, NULL, NULL),
	(23, 16300, 17115, '2018-10-01 18:48:11', 'guest', '2018-10-01 18:48:11', NULL, NULL, NULL),
	(24, 10000, 10500, '2018-10-01 19:13:34', 'guest', '2018-10-01 19:13:34', NULL, NULL, NULL),
	(25, 8000, 8400, '2018-10-01 19:14:21', 'guest', '2018-10-01 19:14:21', NULL, NULL, NULL),
	(26, 4000, 4200, '2018-10-01 20:45:03', 'guest', '2018-10-01 20:45:03', NULL, NULL, NULL);
/*!40000 ALTER TABLE `slip` ENABLE KEYS */;

-- Dumping structure for table tea_shop.slip_item
CREATE TABLE IF NOT EXISTS `slip_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slip_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.slip_item: ~36 rows (approximately)
/*!40000 ALTER TABLE `slip_item` DISABLE KEYS */;
INSERT INTO `slip_item` (`id`, `slip_id`, `product_id`, `qty`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 3, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(2, 1, 5, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(3, 1, 6, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(4, 2, 3, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(5, 2, 12, 3, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(6, 2, 9, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(7, 3, 3, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(8, 3, 5, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(9, 3, 6, 4, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(10, 3, 7, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(11, 4, 3, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(12, 5, 12, 1, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(13, 5, 3, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(14, 5, 5, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(15, 12, 12, 2, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(16, 12, 13, 1, '2018-08-12 15:36:26', 'guest', '2018-08-12 16:22:27', 'guest', NULL, NULL),
	(17, 12, 5, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(18, 13, 12, 3, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(19, 13, 3, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(20, 13, 5, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(21, 14, 4, 2, '2018-07-28 04:46:57', 'guest', '2018-07-28 04:46:57', NULL, NULL, NULL),
	(22, 14, 2, 1, '2018-07-28 04:46:57', 'guest', '2018-07-28 04:46:57', NULL, NULL, NULL),
	(23, 15, 5, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(24, 15, 6, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(25, 15, 7, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(26, 15, 12, 1, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(27, 17, 7, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(28, 17, 6, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(29, 18, 12, 1, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(30, 18, 5, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(31, 18, 6, 1, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(32, 19, 13, 1, '2018-08-12 15:36:26', 'guest', '2018-08-12 16:22:27', 'guest', NULL, NULL),
	(33, 19, 12, 2, '2018-07-29 08:53:38', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(34, 19, 3, 2, '2018-07-28 05:13:28', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(35, 20, 5, 1, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(36, 20, 6, 2, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(37, 20, 7, 1, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(38, 20, 8, 1, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(39, 21, 13, 2, '2018-10-01 18:48:11', 'guest', '2018-08-12 16:22:27', 'guest', NULL, NULL),
	(40, 21, 12, 2, '2018-10-01 18:48:11', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(41, 21, 3, 2, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(42, 21, 5, 1, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(43, 22, 12, 12, '2018-10-01 18:48:11', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(44, 22, 3, 4, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(45, 22, 5, 23, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(46, 22, 6, 12, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(47, 23, 13, 10, '2018-10-01 18:48:11', 'guest', '2018-08-12 16:22:27', 'guest', NULL, NULL),
	(48, 23, 12, 20, '2018-10-01 18:48:11', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(49, 23, 3, 1, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(50, 23, 5, 2, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(51, 23, 6, 2, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(52, 23, 7, 1, '2018-10-01 18:48:11', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(53, 24, 12, 35, '2018-10-02 19:13:34', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(54, 24, 3, 3, '2018-10-02 19:13:34', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(55, 24, 5, 6, '2018-10-02 19:13:34', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(56, 24, 6, 1, '2018-10-02 19:13:34', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(57, 24, 7, 1, '2018-10-02 19:13:34', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(58, 25, 6, 10, '2018-10-02 19:14:21', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(59, 25, 7, 16, '2018-10-02 19:14:21', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(60, 25, 5, 20, '2018-10-02 19:14:21', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(61, 26, 12, 1, '2018-10-01 20:45:03', 'guest', '2018-07-29 08:53:38', NULL, NULL, NULL),
	(62, 26, 3, 1, '2018-10-01 20:45:03', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL),
	(63, 26, 5, 1, '2018-10-01 20:45:03', 'guest', '2018-07-28 05:13:43', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `slip_item` ENABLE KEYS */;

-- Dumping structure for table tea_shop.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `code`, `username`, `password`, `email`, `mobile`, `gender`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, '1', 'root', 'asd123!@', 'root@root.com', '96369079', 'Female', NULL, NULL, '2017-12-23 08:21:14', NULL, NULL, NULL, NULL),
	(6, NULL, 'Aye Chan Thaw', 'asd123!@', 'ayechanthaw@gmail.com', '0987667899', 'Female', '2018-10-01 18:23:33', 'guest', '2018-10-01 18:23:33', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table tea_shop.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table tea_shop.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table tea_shop.user_access
CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `acc_admin_menu_id` int(11) DEFAULT NULL,
  `acc_admin_menu_option_id` int(11) DEFAULT NULL,
  `access_type` enum('none','full','custom') DEFAULT NULL,
  `custom_access` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `special_data` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.user_access: ~11 rows (approximately)
/*!40000 ALTER TABLE `user_access` DISABLE KEYS */;
INSERT INTO `user_access` (`id`, `user_id`, `acc_admin_menu_id`, `acc_admin_menu_option_id`, `access_type`, `custom_access`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `special_data`) VALUES
	(1, 1, 2, 1, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, 1, 2, 2, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 1, 2, 3, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, 1, 5, 4, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 1, 5, 5, 'none', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 1, 5, 6, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, 1, 5, 7, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, 1, 8, 8, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 1, 8, 9, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, 1, 8, 10, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 1, 8, 11, 'full', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_access` ENABLE KEYS */;

-- Dumping structure for table tea_shop.user_permission
CREATE TABLE IF NOT EXISTS `user_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `acc_admin_menu_item_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.user_permission: ~9 rows (approximately)
/*!40000 ALTER TABLE `user_permission` DISABLE KEYS */;
INSERT INTO `user_permission` (`id`, `user_id`, `acc_admin_menu_item_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, 1, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(2, 1, 2, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(3, 1, 3, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(4, 1, 4, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(5, 1, 5, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(6, 1, 6, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(7, 1, 7, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(8, 1, 8, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(9, 1, 9, '2018-10-01 01:19:21', NULL, NULL, NULL, NULL, NULL),
	(11, 6, 3, '2018-10-01 18:23:34', 'guest', '2018-10-01 18:23:34', NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_permission` ENABLE KEYS */;

-- Dumping structure for table tea_shop.user_saved_filter
CREATE TABLE IF NOT EXISTS `user_saved_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `filter_type` varchar(255) DEFAULT NULL,
  `filter_name` varchar(255) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `filters` text,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.user_saved_filter: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_saved_filter` DISABLE KEYS */;
INSERT INTO `user_saved_filter` (`id`, `user_id`, `filter_type`, `filter_name`, `qty`, `filters`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(3, 1, 'product', 'Dim Sum', NULL, '{"name":"dim"}', '2018-07-29 13:36:21', NULL, '2018-07-29 13:36:21', NULL, NULL, NULL);
/*!40000 ALTER TABLE `user_saved_filter` ENABLE KEYS */;

-- Dumping structure for table tea_shop.user_session
CREATE TABLE IF NOT EXISTS `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `login_time` datetime DEFAULT NULL,
  `logout_time` datetime DEFAULT NULL,
  `last_action` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- Dumping data for table tea_shop.user_session: ~50 rows (approximately)
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
INSERT INTO `user_session` (`id`, `user_id`, `token`, `ip_address`, `login_time`, `logout_time`, `last_action`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) VALUES
	(1, 1, '65NAnzfCqB1ROEiuYVnPSgRpFcC5WDHnpy5vGQshFpnav4QSmt', '127.0.0.1', '2018-07-27 16:12:09', '2018-07-27 16:22:17', '2018-07-27 16:12:09', '2018-07-27 16:12:09', 'System', '2018-07-27 16:22:17', 'guest', NULL, NULL),
	(2, 1, 'cR5SEtJJW1RkrGYw32FzO6cqjEcfk7CClFCGY6eCsdn0NXWnZe', '127.0.0.1', '2018-07-27 16:25:36', NULL, '2018-07-27 16:25:36', '2018-07-27 16:25:37', 'System', '2018-07-27 16:25:37', NULL, NULL, NULL),
	(3, 1, 'BWDuDaPyPPkRYD5XXXx3wnZ6s5bgor5HFwvGGud3ENazzQorLD', '127.0.0.1', '2018-07-28 04:44:02', NULL, '2018-07-28 04:44:02', '2018-07-28 04:44:03', 'System', '2018-07-28 04:44:03', NULL, NULL, NULL),
	(4, 1, 'evS2dBY2yq27PI4s5iV2oQtf8qbc4l6oRVCw34G2VEM6Rw3x5t', '127.0.0.1', '2018-07-28 05:12:55', NULL, '2018-07-28 05:12:55', '2018-07-28 05:12:55', 'System', '2018-07-28 05:12:55', NULL, NULL, NULL),
	(5, 1, 'emN1pSLsOSRdeDJa5iuCAEBnzPAqfUGhEnbi0BijMOZQvuj88T', '127.0.0.1', '2018-07-28 05:26:18', NULL, '2018-07-28 05:26:18', '2018-07-28 05:26:18', 'System', '2018-07-28 05:26:18', NULL, NULL, NULL),
	(6, 1, '3aWEzVxCRVCFdBSa8rM9PvREPNqVipRngA58se3vGQ7tHImbi9', '127.0.0.1', '2018-07-28 06:07:04', NULL, '2018-07-28 06:07:04', '2018-07-28 06:07:04', 'System', '2018-07-28 06:07:04', NULL, NULL, NULL),
	(7, 1, 'wWXfcfpRsd3fQZe8auyePZ16GVoNz8dpTCXmlo1kXDgWgfFQlC', '127.0.0.1', '2018-07-28 15:02:39', '2018-07-28 17:00:09', '2018-07-28 15:02:39', '2018-07-28 15:02:41', 'System', '2018-07-28 17:00:09', 'guest', NULL, NULL),
	(8, 1, '5sTDroeSZMEsdC7dKjUutsugP0AZiFsrHBw3lycWnfGJh7ZU8F', '127.0.0.1', '2018-07-28 17:00:12', '2018-07-28 18:00:49', '2018-07-28 17:00:12', '2018-07-28 17:00:13', 'System', '2018-07-28 18:00:49', 'guest', NULL, NULL),
	(9, 1, 'Jplnd4SDCOT0Pj1wflU5IWelg9yQrPuEBM2G44AmSkU0THF5Mz', '127.0.0.1', '2018-07-28 18:00:53', '2018-07-28 18:01:27', '2018-07-28 18:00:53', '2018-07-28 18:00:53', 'System', '2018-07-28 18:01:27', 'guest', NULL, NULL),
	(10, 1, '23N77OC1AiyhVfqtLIZzCq8wJpVKn18QZt7e5wboWEtAQ2x7Xb', '127.0.0.1', '2018-07-28 18:01:31', '2018-07-29 12:06:29', '2018-07-28 18:01:31', '2018-07-28 18:01:31', 'System', '2018-07-29 12:06:30', 'guest', NULL, NULL),
	(11, 1, 'SQSqao3o62LLiJzNw5K0826yOH1NPQhCJl73voz78Vm3SGcMbv', '127.0.0.1', '2018-07-29 12:06:39', NULL, '2018-07-29 12:06:39', '2018-07-29 12:06:40', 'System', '2018-07-29 12:06:40', NULL, NULL, NULL),
	(12, 1, 'VQmyVlPm9SWDvmZqjvYwTG4B3xvKCVxgOfvI9BTMDFzi8Qw1id', '127.0.0.1', '2018-07-29 13:44:37', NULL, '2018-07-29 13:44:37', '2018-07-29 13:44:38', 'System', '2018-07-29 13:44:38', NULL, NULL, NULL),
	(13, 1, '2kseA15CfCdNIriaWpYz8xbGhDDZp0pZU4G3aZjsVH8HPRAMg2', '127.0.0.1', '2018-07-29 14:17:51', NULL, '2018-07-29 14:17:51', '2018-07-29 14:17:52', 'System', '2018-07-29 14:17:52', NULL, NULL, NULL),
	(14, 1, 'SUaxOfDUf13CLrfjunFE9FWjDoKFucDndeLmPs9JeTJiMQ21vl', '127.0.0.1', '2018-07-29 14:20:21', NULL, '2018-07-29 14:20:21', '2018-07-29 14:20:21', 'System', '2018-07-29 14:20:21', NULL, NULL, NULL),
	(15, 1, 'preW737SdkRdJWBjyHrz7xynMEugdUdBF4MU0naWmtmN6uxoIB', '127.0.0.1', '2018-07-29 14:24:02', NULL, '2018-07-29 14:24:02', '2018-07-29 14:24:02', 'System', '2018-07-29 14:24:02', NULL, NULL, NULL),
	(16, 1, '3gm8zFseiNwgrL3CD5OP8sJ9xq05PUHl4KaS4NJVZ5GdbXNIE1', '127.0.0.1', '2018-08-07 16:40:29', NULL, '2018-08-07 16:40:29', '2018-08-07 16:40:30', 'System', '2018-08-07 16:40:30', NULL, NULL, NULL),
	(17, 1, 'SssJQWguY4IfhH6f3ipPpF3Zj5rz2CYRehZpPZUiv7U0FdmYne', '127.0.0.1', '2018-08-12 08:08:58', '2018-08-12 08:51:20', '2018-08-12 08:08:58', '2018-08-12 08:08:59', 'System', '2018-08-12 08:51:20', 'guest', NULL, NULL),
	(18, 1, 'SLWlJxvpy1Ayyo3x4yBwiYpOJ3amdpQfeDp2gUlQgZy8nkTLt5', '127.0.0.1', '2018-08-12 08:52:05', NULL, '2018-08-12 08:52:05', '2018-08-12 08:52:07', 'System', '2018-08-12 08:52:07', NULL, NULL, NULL),
	(19, 1, 'kp2kMgDdexOxj9dJbKC9WrOGBdwuWKWihkU9YCESc8AVerlhae', '127.0.0.1', '2018-08-12 08:53:28', NULL, '2018-08-12 08:53:28', '2018-08-12 08:53:28', 'System', '2018-08-12 08:53:28', NULL, NULL, NULL),
	(20, 1, '0AeG0aHYpd7CZR5dJepbCVXxIkwlGTs3ylKj9yQLSsExmXcQSW', '127.0.0.1', '2018-08-12 08:56:31', NULL, '2018-08-12 08:58:34', '2018-08-12 08:56:31', 'System', '2018-08-12 08:58:34', 'guest', NULL, NULL),
	(21, 1, 'xJFVWqSbNuEupCrOrFQvajs8YMT3TQfIqZlg9kgkN3BQmh3Qn6', '127.0.0.1', '2018-08-12 08:58:39', NULL, '2018-08-12 11:20:56', '2018-08-12 08:58:39', 'System', '2018-08-12 11:20:56', 'guest', NULL, NULL),
	(22, 1, 'tpumtWtbLhER1ONZpTr80wadD49AGV8ivkl9J49yN3s77CeCnN', '127.0.0.1', '2018-08-12 11:20:59', NULL, '2018-08-12 11:34:04', '2018-08-12 11:21:00', 'System', '2018-08-12 11:34:04', 'guest', NULL, NULL),
	(23, 1, 'CkqeMcssrK3VLYLmOCrmJ6ePm6RB2AZq1VhZJrKSD0QjCjOEDN', '127.0.0.1', '2018-08-12 13:55:57', NULL, '2018-08-12 13:58:16', '2018-08-12 13:55:59', 'System', '2018-08-12 13:58:16', 'guest', NULL, NULL),
	(24, 1, 'gmnD4Uks7T4dm0VBo3vWWXzRdnkz4OarYjPEuaNC1cS1vwIhKD', '127.0.0.1', '2018-08-12 15:14:09', NULL, '2018-08-12 16:32:11', '2018-08-12 15:14:10', 'System', '2018-08-12 16:32:11', 'guest', NULL, NULL),
	(25, 1, 'J4qpfwRFJf0LKom5pN2u4B7hlpasBRib58XQmnXrqYmWhlMiDg', '127.0.0.1', '2018-08-12 16:32:15', NULL, '2018-08-12 16:54:23', '2018-08-12 16:32:15', 'System', '2018-08-12 16:54:23', 'guest', NULL, NULL),
	(26, 1, '1k6YQSmrP6uQWpUS9C6aYOy8XT9nB7Rs8DgR3AMpNrEz81ie4s', '127.0.0.1', '2018-09-10 14:28:30', NULL, '2018-09-10 14:50:01', '2018-09-10 14:28:30', 'System', '2018-09-10 14:50:01', 'guest', NULL, NULL),
	(27, 1, 'lXTn7Cp1AMO6XJHr2GudWN4rAOoCIBCkG96p65RHzH5l1O835V', '127.0.0.1', '2018-09-10 14:50:03', NULL, '2018-09-10 14:51:16', '2018-09-10 14:50:04', 'System', '2018-09-10 14:51:16', 'guest', NULL, NULL),
	(28, 1, '6qjgWsSaKb7SYD5tjywkCJjR4qRlXO1WWT8bKH0dhMPZlBSZep', '127.0.0.1', '2018-09-10 14:51:18', NULL, '2018-09-10 15:03:56', '2018-09-10 14:51:18', 'System', '2018-09-10 15:03:56', 'guest', NULL, NULL),
	(29, 1, 'oiFBaczjiJX8UO1IQqRf7YVyFzMUzMKff14k69j9Ew4DyypWKO', '127.0.0.1', '2018-09-10 15:03:58', NULL, '2018-09-10 15:05:06', '2018-09-10 15:03:59', 'System', '2018-09-10 15:05:06', 'guest', NULL, NULL),
	(30, 1, '5sO0MgSjxbDmILuS5gAOhnKFvKZ2sELIItOpfX49XZPSQdDVHn', '127.0.0.1', '2018-09-10 16:39:14', '2018-09-10 18:08:04', '2018-09-10 16:39:20', '2018-09-10 16:39:15', 'System', '2018-09-10 18:08:04', 'guest', NULL, NULL),
	(31, 1, 'v45FSLknTtCfoxMJ9M80Jz4kh9ZXd7lKW7ekwjPWX01TvJe58k', '127.0.0.1', '2018-09-10 18:08:10', NULL, '2018-09-10 18:09:25', '2018-09-10 18:08:10', 'System', '2018-09-10 18:09:26', 'guest', NULL, NULL),
	(32, 1, 'iPICGcAPcd6Dah0PbUoUw1Soz3ULvwLchdvFHQnkA9LUMU504n', '127.0.0.1', '2018-09-11 15:06:39', NULL, '2018-09-11 16:27:08', '2018-09-11 15:06:40', 'System', '2018-09-11 16:27:08', 'guest', NULL, NULL),
	(33, 1, '1TzrdUpxrV9w5Ie8CLRebPpOysZuGnl91eEvZ7VYI5zchZx2gX', '127.0.0.1', '2018-09-11 17:36:16', NULL, '2018-09-11 17:43:19', '2018-09-11 17:36:16', 'System', '2018-09-11 17:43:19', 'guest', NULL, NULL),
	(34, 1, '1C4nqe5Dg6VrqeHjMijsMPgTDQ3wyIFarJDRtQ5DITHseF4gUB', '127.0.0.1', '2018-09-13 15:48:56', NULL, '2018-09-13 15:51:58', '2018-09-13 15:48:56', 'System', '2018-09-13 15:51:58', 'guest', NULL, NULL),
	(35, 1, 'GbmAebRSQcZxlGFWsvAREDSh3zHKphl6iB1T17vOy1aIhRourv', '127.0.0.1', '2018-09-13 15:52:02', NULL, '2018-09-13 16:22:53', '2018-09-13 15:52:02', 'System', '2018-09-13 16:22:54', 'guest', NULL, NULL),
	(36, 1, 'KK5EPWnDXAw9IbWYoULuSUgcqJJsFtPBlkQzQa0pPPqZlUpnjT', '127.0.0.1', '2018-09-13 17:46:44', NULL, '2018-09-13 17:46:44', '2018-09-13 17:46:45', 'System', '2018-09-13 17:46:45', NULL, NULL, NULL),
	(37, 1, '9TXinMx4svU41ZZM4r8ejs4ji4jEgOXcgIFXnJTsFaD8O24VZs', '127.0.0.1', '2018-09-16 15:02:21', '2018-09-16 15:09:17', '2018-09-16 15:09:05', '2018-09-16 15:02:22', 'System', '2018-09-16 15:09:17', 'guest', NULL, NULL),
	(38, 1, '9FSv9UyCbUC4GI9iPqIyaeZpsKeLwU0ZpVqzqGWfijmVbOYlMC', '127.0.0.1', '2018-09-16 15:09:23', '2018-09-16 15:09:38', '2018-09-16 15:09:23', '2018-09-16 15:09:23', 'System', '2018-09-16 15:09:38', 'guest', NULL, NULL),
	(39, 1, 'spqM2lFf545Fc9UswmjE3LZaQJ6QJ2n4N8HrAVWMkwmU2UIHQK', '127.0.0.1', '2018-09-16 15:09:43', '2018-09-16 15:14:40', '2018-09-16 15:12:19', '2018-09-16 15:09:43', 'System', '2018-09-16 15:14:40', 'guest', NULL, NULL),
	(40, 1, 'WKjlV6TkV7RtpiUvuuxnjF8R7rhPjaV3uiKuspXmgJIspFG96k', '127.0.0.1', '2018-09-16 15:14:54', NULL, '2018-09-16 15:48:07', '2018-09-16 15:14:54', 'System', '2018-09-16 15:48:07', 'guest', NULL, NULL),
	(41, 1, 'flttqWUKI7tNYEMqYApXCQwzGvfoQAEkHVDZ5RoTUVphdNN4yX', '127.0.0.1', '2018-09-18 18:48:52', NULL, '2018-09-18 19:09:23', '2018-09-18 18:48:53', 'System', '2018-09-18 19:09:23', 'guest', NULL, NULL),
	(42, 1, 'YyiuQ20jzLhi8lcGmqUkQhiWpdeHGyCJSlm1vluPYG0COsaE1b', '127.0.0.1', '2018-09-30 16:52:52', NULL, '2018-09-30 17:39:14', '2018-09-30 16:52:53', 'System', '2018-09-30 17:39:14', 'guest', NULL, NULL),
	(43, 1, 'sb0Y1eStAHoMDZnaTMu1ZenT3tRxSMSRYX6BGckY7oykFLCJo0', '127.0.0.1', '2018-09-30 17:39:16', NULL, '2018-09-30 17:56:21', '2018-09-30 17:39:16', 'System', '2018-09-30 17:56:21', 'guest', NULL, NULL),
	(44, 1, 'dCTbaD3lGKJFanNQVcWf6AYiniYZZX5NI9dMwYnD1S66F8bfDC', '127.0.0.1', '2018-09-30 17:56:22', '2018-09-30 18:23:18', '2018-09-30 18:21:04', '2018-09-30 17:56:22', 'System', '2018-09-30 18:23:18', 'guest', NULL, NULL),
	(45, 1, 'PkTOf1vGph71SvQ9WDLileAZ34up1YNJoSvUlx4YMhMHwsoArO', '127.0.0.1', '2018-09-30 18:23:27', '2018-09-30 18:23:46', '2018-09-30 18:23:27', '2018-09-30 18:23:27', 'System', '2018-09-30 18:23:46', 'guest', NULL, NULL),
	(46, 2, '1HaoNMPYaehkU2NmvYK2Bhb6BTKFQXvEO2R4t8vomKxNl2EkPc', '127.0.0.1', '2018-09-30 18:33:12', NULL, '2018-09-30 18:59:03', '2018-09-30 18:33:12', 'System', '2018-09-30 18:59:03', 'guest', NULL, NULL),
	(47, 1, 'SljEWk81wm1oQMX6q02p31roeblrUgz8YW6KRt1iVVU4DlCHZL', '127.0.0.1', '2018-09-30 18:59:46', NULL, '2018-09-30 19:01:55', '2018-09-30 18:59:46', 'System', '2018-09-30 19:01:55', 'guest', NULL, NULL),
	(48, 1, 'WPiSNSwdZknBJ9eJxFf2HYqCqOraXENXnU9ufEa2zYR3raTRPZ', '127.0.0.1', '2018-09-30 19:02:06', '2018-09-30 19:02:26', '2018-09-30 19:02:08', '2018-09-30 19:02:06', 'System', '2018-09-30 19:02:26', 'guest', NULL, NULL),
	(49, 2, '23HgIJBWEJ4kZu18Pxu7BAHVW5SxAXsc9mukuusmWXYHntdxEI', '127.0.0.1', '2018-09-30 19:02:36', '2018-09-30 19:06:54', '2018-09-30 19:06:51', '2018-09-30 19:02:36', 'System', '2018-09-30 19:06:54', 'guest', NULL, NULL),
	(50, 1, 'SDJ1HOGXFvLFciBnFk5ckGhF5ndlffZjPNPlyhNYLJelB0oZmT', '127.0.0.1', '2018-09-30 19:07:13', NULL, '2018-09-30 19:29:32', '2018-09-30 19:07:13', 'System', '2018-09-30 19:29:32', 'guest', NULL, NULL),
	(51, 1, 'yqUVAS9pTlAEddYdRnwserezA0lqHyjznuDNLUdlfxinMxipAu', '127.0.0.1', '2018-10-01 18:02:52', '2018-10-01 18:23:44', '2018-10-01 18:22:13', '2018-10-01 18:02:53', 'System', '2018-10-01 18:23:44', 'guest', NULL, NULL),
	(52, 6, 'X9FQnXnmUDBpJTvpBZQxaq1Af6pHfeviHBlW9jifSmiqrEdW1m', '127.0.0.1', '2018-10-01 18:23:57', '2018-10-01 18:24:50', '2018-10-01 18:24:13', '2018-10-01 18:23:57', 'System', '2018-10-01 18:24:50', 'guest', NULL, NULL),
	(53, 1, 'xGKMMwbSq84ZrprazrYq5rc3QBTI3MZWM9rNxC4UfrTzjwqSRw', '127.0.0.1', '2018-10-01 18:25:00', '2018-10-01 19:17:27', '2018-10-01 19:07:40', '2018-10-01 18:25:00', 'System', '2018-10-01 19:17:27', 'guest', NULL, NULL),
	(54, 1, 'Hdhr6WElx4GSfB7MIs7m6buhnVRQhGaJnkX8gfCD6jHBDOjMn4', '127.0.0.1', '2018-10-01 19:17:43', NULL, '2018-10-01 20:44:49', '2018-10-01 19:17:43', 'System', '2018-10-01 20:44:50', 'guest', NULL, NULL);
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
