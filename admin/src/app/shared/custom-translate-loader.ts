import { TranslateModule , TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Injectable } from '@angular/core';
import { APPCONFIG } from '../config';
import { Observable } from 'rxjs/Observable';
import { HttpService } from "./http.service";
import { Http, HttpModule, Headers, RequestOptions, RequestMethod, Response} from '@angular/http';


@Injectable()
export class CustomTranslateLoader implements TranslateLoader  {

	constructor(private http: Http) { console.log("customer loader initialized");  }
	/*
	getTranslation(lang: string): Observable<any> {
		return Observable.create(observer => {
			this.http.get('generic/get_translate?languague=').subscribe((data) => {
				let res = data.json();
				observer.next(res);
				observer.complete();
			});
		});
	}
	*/

	getTranslation(lang: string): Observable<any> {
		var apiAddress = APPCONFIG.baseUrl + "/generic/get_translate?lang=" + lang;
		return Observable.create(observer => {
			this.http.get(apiAddress).subscribe((res) => {
				observer.next(res);
				observer.complete();
			},
			error => {
				//  failed to retrieve from api, switch to local
				this.http.get("/assets/i18n/en.json").subscribe((res) => {
					observer.next(res);
					observer.complete();
				})
			});
		});
	}

}