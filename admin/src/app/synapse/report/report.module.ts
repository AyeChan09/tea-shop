import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleComponent } from './sale/sale.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { SaleDetailComponent } from './sale-detail/sale-detail.component';

export const routes = [
  { path: 'sale', component: SaleComponent },
  { path: 'sale_detail', component: SaleDetailComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SaleComponent, SaleDetailComponent]
})
export class ReportModule { }
