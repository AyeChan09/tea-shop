import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'app-sale-detail',
  templateUrl: './sale-detail.component.html',
  styleUrls: ['./sale-detail.component.scss']
})
export class SaleDetailComponent implements OnInit {
  start_date: any;
  end_date: any;
  recordData: any = [];
  search: any = {};
  total_profit: number = 0;

  constructor(
    private httpService: HttpService,
    private parserFormatter: NgbDateParserFormatter
  ) { }

  ngOnInit() {
  }

  getData() {
    let param: any = {};
    let start = this.parserFormatter.format(this.search.start_date);
    param.start_date = moment(start).format("YYYY-MM-DD");
    let end = this.parserFormatter.format(this.search.end_date);
    param.end_date = moment(end).format("YYYY-MM-DD");
    this.httpService.get("/report/get_report_detail", param).subscribe((data) => {
      let res = data.json();
      this.recordData = res.data;
      this.total_profit = res.total_profit;
    });
  }

  clearFilter() {
    this.search = {start_date: null, end_date: null};
  }

}
