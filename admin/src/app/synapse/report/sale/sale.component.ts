import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../shared/http.service';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.scss']
})
export class SaleComponent implements OnInit {
  start_date: any;
  end_date: any;
  recordData: any = [];
  search: any = {};
  total_profit: number = 0;

  constructor(
    private httpService: HttpService,
    private parserFormatter: NgbDateParserFormatter
  ) { }

  ngOnInit() {
  }

  getData() {
    let param: any = {};
    let start = this.parserFormatter.format(this.search.start_date);
    param.start_date = moment(start).format("YYYY-MM-DD");
    this.start_date = param.start_date;
    let end = this.parserFormatter.format(this.search.end_date);
    param.end_date = moment(end).format("YYYY-MM-DD");
    this.end_date = param.end_date;
    this.httpService.get("/report/get_report", param).subscribe((data) => {
      let res = data.json();
      this.recordData = res.data;
      this.total_profit = res.total_profit;
    });
  }

  clearFilter() {
    this.search = {start_date: null, end_date: null};
  }

  exportEXCEL() {
    if(this.start_date != undefined) {
      let param: any = {};
      param['start_date'] = this.start_date;
      param['end_date'] = this.end_date;
      this.httpService.newPage("/report/sale_report_excel", param);
    }    
  }

}
