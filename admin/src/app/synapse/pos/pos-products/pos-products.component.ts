import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { HttpService } from '../../../shared/http.service';

@Component({
  selector: 'app-pos-products',
  templateUrl: './pos-products.component.html',
  styleUrls: ['./pos-products.component.scss']
})
export class PosProductsComponent implements OnInit {
  @Output() productClicked = new EventEmitter<string>();
  data: any = [];
  currentPage: number = 1;
  totalTableData: number = 0;
  // pageLimitOptions: any = APPCONFIG.recordPerPage;
  pageLimitOptions: any = [10, 20, 30];
  pageLimit: number = this.pageLimitOptions[0];
  order: any = { "by": "created_at", "dir": "desc" };
  search: any = {};

  constructor(
    public httpService: HttpService,
  ) { }

  ngOnInit() {
    this.getPage(1, 1);
  }

  getPage(page: number, filter = 0) {
    this.currentPage = page;
    this.data = this.loadData().do(res => {
      let ret = res.json();
      if (filter == 1) {
        this.totalTableData = ret.total;
      }
    })
      .map(res => res.json().data);

    return 1; //return for callback purpose, if not it'll be ignored by subsribe in confirm
  }
  loadData() {
    let param: any = {};
    param.limit = this.pageLimit;
    param.page = this.currentPage;
    param.search = this.search;
    param.order = this.order;
    return this.httpService.get("/product/product", param);
  }

  itemClicked(record) {
    this.productClicked.emit(record);
  }

}
