import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { HttpService } from '../../shared/http.service';
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
    selector: 'app-pos',
    templateUrl: './pos.component.html',
    styleUrls: ['./pos.component.scss'],
  })
  
  export class PosComponent implements OnInit {
    carts: any = [];
    subtotal: number = 0;
    grandtotal: number = 0;
    tax: number = 0;

    constructor(
        public router: Router,
        private activatedRoute: ActivatedRoute,
        private ngbModal: NgbModal,
        private httpService: HttpService,
        public translate: TranslateService,
        private modalService: NgbModal,
        private parserFormatter: NgbDateParserFormatter
      ) {
          
    }

    ngOnInit() {
       
    }

    innerProductClicked(product) {
        console.log('product=>', product);
        let qty = 1;
        for (let i = 0; i < this.carts.length; i++) {
            if (this.carts[i].id == product.id) {
                qty = this.carts[i].qty;
                qty++;
                this.carts[i].qty = qty;
                this.subtotal += this.carts[i].price; 
                this.tax = this.subtotal * 0.05;
                this.grandtotal = this.subtotal + this.tax;
            }
        }
        if (qty == 1) {
            product.qty = qty;
            this.subtotal += product.price;
            this.tax = this.subtotal * 0.05;
            this.grandtotal = this.subtotal + this.tax;
            this.carts.push(product);
        }
    
        // product.qty = 1;
        // product.discount = 0;
        // this.carts.push(product);
    }

    add(product) {
        let qty = 1;
        for (let i = 0; i < this.carts.length; i++) {
            if (this.carts[i].id == product.id) {
                qty = this.carts[i].qty;
                qty++;
                this.subtotal += this.carts[i].price;
                this.tax = this.subtotal * 0.05;
                this.grandtotal = this.subtotal + this.tax;
                this.carts[i].qty = qty;
            }
        }
    }

    substract(product) {
        console.log('cart=>', this.carts);
        let qty = 1;
        for (let i = 0; i < this.carts.length; i++) {
            if (this.carts[i].id == product.id) {
                qty = this.carts[i].qty;
                if(qty == 1) {
                    let index = this.carts.indexOf(product);
                    console.log('index=>', index);
                    this.subtotal -= this.carts[i].price;
                    this.tax = this.subtotal * 0.05;
                    this.grandtotal = this.subtotal + this.tax;
                    this.carts.splice(index, 1);
                }
                qty--;
                this.subtotal -= this.carts[i].price;
                this.tax = this.subtotal * 0.05;
                this.grandtotal = this.subtotal + this.tax;
                this.carts[i].qty = qty;
            }
        }
    }

    doDeleteCartItem(index) {
        let price = this.carts[index].qty * this.carts[index].price;
        console.log('price=.', price);
        this.subtotal -= price; 
        this.tax = this.subtotal * 0.05;
        this.grandtotal = this.subtotal + this.tax;
        this.carts.splice(index, 1);
    }
    
    saveCart()
    {
        let parameter: any = {};
        parameter.cart = this.carts;
        parameter.subtotal = this.subtotal;
        parameter.grand_total = this.grandtotal;
        let rt: any = this.httpService.post("/slip/slip", { 'parameter': parameter });
        rt.subscribe((data) => {
          let res = data.json();
          
          let message = "failed";
          let action = "close";
          if (!res.error) {
            // message = "successful";
            // swal(
            //     'Alert',
            //     'Data Have Been Saved',
            //     'success'
            //   )
            this.carts = [];
            this.subtotal = 0;
            this.tax = 0;
            this.grandtotal = 0;
          } else {
          }
        });
    }

  }