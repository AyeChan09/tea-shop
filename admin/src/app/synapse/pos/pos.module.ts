import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PosProductsComponent } from './pos-products/pos-products.component';
import { PosComponent } from './pos.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

export const routes=[
  { path:'pos', component:PosComponent},
  { path:'product', component:PosProductsComponent},
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [PosComponent, PosProductsComponent]
})
export class PosModule { }
