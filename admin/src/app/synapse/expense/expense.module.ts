import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../../shared/shared.module";

import { ExpenseFormComponent } from './expense-form/expense-form.component';
import { ExpenseListComponent } from './expense-list/expense-list.component';
import { ExpenseItemComponent } from './expense-item/expense-item.component';
import { ExpenseViewComponent } from './expense-view/expense-view.component';

export const routes = [
  { path: 'expense_list', component: ExpenseListComponent },
  { path: 'expense_form', component: ExpenseFormComponent },
  { path: 'expense_form/:id', component: ExpenseFormComponent },
  { path: 'expense_view/:id', component: ExpenseViewComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExpenseFormComponent, ExpenseListComponent, ExpenseItemComponent, ExpenseViewComponent],
  entryComponents:[ExpenseItemComponent]
})
export class ExpenseModule { }
