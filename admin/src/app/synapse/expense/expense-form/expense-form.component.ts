import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router, ActivatedRoute,Params } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from "../../../shared/http.service";
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import swal from 'sweetalert2';
import { ExpenseItemComponent } from '../expense-item/expense-item.component';

@Component({
  selector: 'app-expense-form',
  templateUrl: './expense-form.component.html',
  styleUrls: ['./expense-form.component.scss']
})
export class ExpenseFormComponent implements OnInit {
  public recordForm : any = FormGroup;
  data : any = [];  
  private sub: any;
  id : number = 0;  
  expense_total: number = 0;
  
  public items: FormArray;
  
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute : ActivatedRoute,
    private location : Location,
    public httpService : HttpService,
    public translate: TranslateService,
    public router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      if(params.hasOwnProperty("id")){
        this.id = params['id']; 
      } 
    });
    this.buildForm();
    if(this.id !=0){
      this.httpService.get("/expense/expense/"+this.id).subscribe((data) =>{
        let res = data.json();

        this.data = res.data;
        this.recordForm.patchValue(this.data);
        let items: any[] = res.data.items;
        
        items.forEach(values => {
          this.addItem(values);
        });
      });
    }
  }

  buildForm(){
    if(this.id == 0){      
      this.recordForm = this.formBuilder.group({
        expense_no: [''],
        total: ['', Validators.required],
        remark: [''],
        items: this.formBuilder.array([]),
      });
    }else{      
      this.recordForm = this.formBuilder.group({
        expense_no: [''],
        total: ['', Validators.required],
        remark: [''],
        items: this.formBuilder.array([]),
      });
    }
  }

  onSubmit({value, valid}) {   
    if(valid == false){
      this.validateforms();
    }else{
        let parameter : any = {};
        parameter = value;
        console.log('parameter=>', parameter);
        if(this.id == 0){    
          let rt : any = this.httpService.post("/expense/expense", parameter);
          rt.subscribe((data) =>{
              let res = data.json();
              let message = "failed";
              let action = "close";
              if(!res.error){
                  message = "successful"; 
                  this.doBack(); 
              }else{
              }
          });
        }
        else{
          let rt : any =  this.httpService.put("/expense/expense/" + this.id, parameter);
          rt.subscribe((data) =>{
            let res = data.json();
            let message = "failed";
            let action = "close";
            if(!res.error){
                message = "successful";
                this.doBack();           
            }else{
            }
          });
        }
    }
  }

  doBack() {
    this.location.back();
  }

  validateforms() {
    Object.keys(this.recordForm.controls).forEach(field => {
      const control = this.recordForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    let title: any = this.translate.get("Invalid!");
    let text: any = this.translate.get("Fields Are Required");
    swal({
      title: title.value,
      text: text.value,
      type: 'info',
      confirmButtonColor: '#0CC27E',
      confirmButtonText: 'Ok',
    }).then(function (isConfirm) {
      if (isConfirm) {
        swal.close();
      }
    },function(dismiss) {
      swal.close();
    });
  }

  addItem(item: any = {}) {
    console.log('item in addParetn=>', item);
    this.items = <FormArray>this.recordForm.get('items');
    this.items.push(this.formItem(item));
    console.log('items in additem=>', this.items);
  }

  addItemForm() {
    let modalRef = this.modalService.open(ExpenseItemComponent, { size: "lg" });
    // modalRef.componentInstance.product = this.product_outlet_records;
    modalRef.result.then((result) => {
      console.log('ItemData=>', result);
      if(result) {
        this.addItem(result);
        this.expense_total += result.total;
      }      
    });
  }

  formItem(item: any = {}): FormGroup {
    let itemForm = this.formBuilder.group({
      id: item.id,
      item_name: [item.item_name, Validators.required],
      price: [item.price],
      qty: [item.qty],
      total: [item.total],
    });
    return itemForm;
  }

}
