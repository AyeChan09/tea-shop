import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpService } from '../../../shared/http.service';

@Component({
  selector: 'app-expense-view',
  templateUrl: './expense-view.component.html',
  styleUrls: ['./expense-view.component.scss']
})
export class ExpenseViewComponent implements OnInit {
  data : any = [];
  id : number = 0;
  private sub: any;

  constructor(
    private activatedRoute : ActivatedRoute,
    public httpService : HttpService,
    private location : Location,
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      if(params.hasOwnProperty("id")){
        this.id = params['id']; 
      } 
    });
    if(this.id !=0){
      this.httpService.get("/expense/expense/"+this.id).subscribe((data) =>{
        let res = data.json();

        this.data = res.data;
        let items: any[] = res.data.items;
      });
    }
  }

  doBack() {
    this.location.back();
  }

}
