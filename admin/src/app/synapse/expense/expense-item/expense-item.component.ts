import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute,Params } from '@angular/router';
import { Location } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { HttpService } from "../../../shared/http.service";
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import swal from 'sweetalert2';

@Component({
  selector: 'app-expense-item',
  templateUrl: './expense-item.component.html',
  styleUrls: ['./expense-item.component.scss']
})
export class ExpenseItemComponent implements OnInit {
  public recordForm : any = FormGroup;
  data : any = [];  
  private sub: any;
  id : number = 0; 
  item_price: number = 0;
  item_qty: number = 0;
  item_total: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute : ActivatedRoute,
    private location : Location,
    public httpService : HttpService,
    public translate: TranslateService,
    public ngbActiveModal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      if(params.hasOwnProperty("id")){
        this.id = params['id']; 
      } 
    });
    this.buildForm();
    if(this.id !=0){
      this.httpService.get("/expense/item/"+this.id).subscribe((data) =>{
        let res = data.json();

        this.data = res.data;
      });
    }
  }

  buildForm(){
    if(this.id == 0){      
      this.recordForm = this.formBuilder.group({
        item_name:['',Validators.required], 
        price: ['',Validators.required],
        qty:['', Validators.required],
        total: [''],
      });
    }else{      
      this.recordForm = this.formBuilder.group({
        item_name:['',Validators.required], 
        price: ['',Validators.required],
        qty:['', Validators.required],
        total: [''],
      });
    }
  }

  sum() {
    this.item_total = this.item_price * this.item_qty;
  }

  onSubmit({value, valid}) {   
    if(valid == false){
      this.validateforms();
    }else{
        let parameter : any = {};
        parameter = value;
        console.log('parameter=>',parameter);
        this.ngbActiveModal.close(parameter);
    }
  }

  doCancel() {
    this.ngbActiveModal.close();
  }

  validateforms() {
    Object.keys(this.recordForm.controls).forEach(field => {
      const control = this.recordForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    let title: any = this.translate.get("Invalid!");
    let text: any = this.translate.get("Fields Are Required");
    swal({
      title: title.value,
      text: text.value,
      type: 'info',
      confirmButtonColor: '#0CC27E',
      confirmButtonText: 'Ok',
    }).then(function (isConfirm) {
      if (isConfirm) {
        swal.close();
      }
    },function(dismiss) {
      swal.close();
    });
  }

}
