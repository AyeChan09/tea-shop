import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { DashboardComponent } from './dashboard/dashboard.component';

import { SynapseComponent } from './synapse.component';
import { LoginComponent } from "./authentication/login/login.component";

export const routes: Routes = [
    { path: 'login', component:LoginComponent, data: { breadcrumb: 'Login' } },
    {
        path: '', 
        component: SynapseComponent,
        children:[
            { path: 'dashboard',component:DashboardComponent},            
            { path: 'product', loadChildren: 'app/synapse/product/product.module#ProductModule', data: { breadcrumb: 'Product'}},
            { path: 'expense', loadChildren: 'app/synapse/expense/expense.module#ExpenseModule', data: { breadcrumb: 'Expense'}},
            { path: 'user', loadChildren: 'app/synapse/user/user.module#UserModule', data: { breadcrumb: 'User'}},
            { path: 'pos', loadChildren: 'app/synapse/pos/pos.module#PosModule', data: { breadcrumb: 'Pos'}},
            { path: 'report', loadChildren: 'app/synapse/report/report.module#ReportModule', data: { breadcrumb: 'Report'}},
        ]
    },
    
    
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);