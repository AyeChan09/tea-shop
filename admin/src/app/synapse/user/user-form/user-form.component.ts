import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { Location } from '@angular/common';
import { HttpService } from '../../../shared/http.service';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  public loading = false;
  public recordForm : any = FormGroup;
  records: any = [];
  data : any = {};  
  current_user: any = {};
  private sub: any;
  id : number = 0;  
  gender: any = ['Male', 'Female'];
  isDuplicate: boolean = false;
  current_password: string;
  isUpdate: boolean = false;
  
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute : ActivatedRoute,
    private location : Location,
    public httpService : HttpService,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      if(params.hasOwnProperty("id")){
        this.id = params['id']; 
      } 
    });
    this.buildForm();
    if(this.id != 0){
      this.isUpdate = true;
      this.httpService.get("/auth/auth/"+this.id).subscribe((data) =>{
        let res = data.json();

        this.data = res.data;
        this.current_password = this.data.password;
        // PASSWORD
        this.recordForm.patchValue(this.data);
      });
    }
    this.httpService.get("/auth/get_old_record").subscribe((data) =>{
      let res = data.json();
      this.records = res;
      console.log('records=>', this.records);
    });
  }

  buildForm(){
    if(this.id == 0){      
      this.recordForm = this.formBuilder.group({
        username:['',Validators.required], 
        mobile: ['',Validators.required],
        gender:['',Validators.required],
        email:['', Validators.compose([Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],    
        password:['', Validators.compose([Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)])],
      });
    }else{      
      this.recordForm = this.formBuilder.group({
        username:['',Validators.required], 
        mobile: ['',Validators.required],
        gender:['',Validators.required],
        email:['', Validators.compose([Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],    
        password:['', Validators.compose([Validators.required, Validators.minLength(8),Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/)])],
      });
    }
  }

  onSubmit({value, valid}) {  
    this.isDuplicate = false;
    if(valid == false){
      this.validateforms();
    }else{
      this.loading = true;
      let parameter : any = {};
      parameter = value;
      if(this.id == 0){    
        this.records.forEach(user => {
          if(user.username == parameter['username'] || user.email == parameter['email']) {
            this.isDuplicate = true;
          }
        })
        if(this.isDuplicate) {
          swal( "Warning!", "Name or Email is already exited.", "warning");
          this.loading = false;
        } else {
          let rt : any = this.httpService.post("/auth/auth", parameter);
          rt.subscribe((data) =>{
              this.loading = false;  
              let res = data.json();
              if(!res.error){
                swal( "Successful!", "User created.", "success");
                this.doBack();
              }
          });
        }
      }
      else{
        if(value.password == this.current_password)
          parameter['status'] = 'unchange';
        else
          parameter['status'] = 'change';
        this.records.forEach(user => {
          if(user.username == parameter['username']) {
            if(parameter['email'] == this.data['email'] && parameter['username'] == this.data['username'])
              this.isDuplicate = false;
            else 
              this.isDuplicate = true;
          }
        }) 
        if(this.isDuplicate) {
          swal( "Warning!", "Name or Email is already exited.", "warning");
          this.loading = false;
        } else {
          let rt : any =  this.httpService.put("/auth/auth/" + this.id, parameter);
          rt.subscribe((data) =>{
            let res = data.json();
            if(!res.error){
              this.loading = false;  
              swal( "Successful!", "User updated.", "success");
              this.doBack();
            }
          });
        }
      }
    }
  }

  validateforms() {
    Object.keys(this.recordForm.controls).forEach(field => {
      const control = this.recordForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    let title: any = this.translate.get("Invalid!");
    let text: any = this.translate.get("Fields Are Required");
    swal({
      title: title.value,
      text: text.value,
      type: 'info',
      confirmButtonColor: '#0CC27E',
      confirmButtonText: 'Ok',
    }).then(function (isConfirm) {
      if (isConfirm) {
        swal.close();
      }
    },function(dismiss) {
      swal.close();
    });
  }

  doBack(){
      this.location.back();
  }
}
