import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from "../../shared/shared.module";

import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

export const routes = [
  { path: 'user_list', component: UserListComponent },
  { path: 'user_form', component: UserFormComponent },
  { path: 'user_form/:id', component: UserFormComponent },
  { path: 'change_password/:id', component: ChangePasswordComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UserListComponent, UserFormComponent, ChangePasswordComponent]
})
export class UserModule { }