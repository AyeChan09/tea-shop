import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpService } from "../../../shared/http.service";
import { TranslateService } from '@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  public recordForm: any = FormGroup;
  id: number = 0;
  private sub: any;
  data: any = {};

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private location: Location,
    public httpService: HttpService,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe(params => {
      if (params.hasOwnProperty("id")) {
        this.id = params['id'];
        console.log('id=>', this.id);
        this.httpService.get("/auth/auth/"+this.id).subscribe((data) =>{
          let res = data.json();
  
          this.data = res.data;
          console.log('data=>', this.data);
        });
      }
    });

    this.buildForm();
  }

  buildForm(){
    this.recordForm = this.formBuilder.group({
      current_password:['', Validators.required], 
      new_password: ['', Validators.compose([Validators.required, Validators.minLength(6),Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$/)])],
      confirm_password:['', Validators.compose([Validators.required, Validators.minLength(6),Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,}$/)])],
    });
  }

  onSubmit({value, valid}) {
    if(valid){

      let parameter : any = {};
      parameter = value;

      if(this.recordForm.value.new_password != this.recordForm.value.confirm_password) {
        swal("Notice!", "Your confirm password must be same to new password.", "info");
      } else if (parameter['current_password'] != this.data.password) {
        swal('Error!', 'Your current password is wrong.', 'error');
      }else {
        let rt : any =  this.httpService.post("/auth/change_password/" + this.id, parameter);
        rt.subscribe((data) =>{
          let res = data.json();
          let title;
          let type;
          if(res.error){
            title = "Error!";
            type = "error";
          }else{
            title = "Successful!";
            type = "success";
            swal(title, res.message, type);
          }
        });
      }     
    }
  }

  validateforms() {
    Object.keys(this.recordForm.controls).forEach(field => {
      const control = this.recordForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    let title: any = this.translate.get("Invalid!");
    let text: any = this.translate.get("Fields Are Required");
    swal({
      title: title.value,
      text: text.value,
      type: 'info',
      confirmButtonColor: '#0CC27E',
      confirmButtonText: 'Ok',
    }).then(function (isConfirm) {
      if (isConfirm) {
        swal.close();
      }
    },function(dismiss) {
      swal.close();
    });
  }

  doBack(){
      this.location.back();
  }

}
