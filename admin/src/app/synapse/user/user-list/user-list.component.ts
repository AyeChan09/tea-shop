import { Component, OnInit } from '@angular/core';
import { APPCONFIG } from '../../../config';
import { Router } from '@angular/router';
import { HttpService } from "../../../shared/http.service";
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  private allow_add = false;
  private allow_edit = false;
  private allow_delete = false;
  private allow_view = false;

  data: any = [];
  selectedRecord: any = {};
  currentPage: number = 1;
  totalTableData: number = 0;
  pageLimitOptions: any = APPCONFIG.recordPerPage;
  pageLimit: number = this.pageLimitOptions[0];
  selections: any = {};
  order: any = { "by": "created_at", "dir": "desc" };

  search: any = {};
  pagination: any = {};

  currentFunction: string = "user";
  currentRoute: string = this.router.url;
  savedFilter: any = [];
  activeFilterId: number = 0;
  activeFilterName: string = "";
  tableHeader: any = [];
  user: number;

  constructor(
    public router: Router,
    public httpService: HttpService,
    public translate: TranslateService
  ) { 
    let id = localStorage.getItem("user");
    this.httpService.post("/generic/get_access", { route: this.currentRoute, user_id:  id}).subscribe((data) => {
      let res = data.json();
      let array: Array<any> = res.data
      array.forEach(item => {
        let allow = true;
        if (item.access_type === "none") {
          allow = false;
        }
        this[item.name] = allow;
      });
    });
    this.httpService.post("/generic/get_header", { list_name: this.currentFunction }).subscribe((data) => {
      let res = data.json();
      this.tableHeader = res.data;
      console.log('header=>', this.tableHeader);
    });
    this.loadData();
    this.user = parseInt(localStorage.getItem("user"));
  }

  ngOnInit() {
    this.getPage(1, 1);
    this.getSavedFilter();
  }

  clearFilter() {
    this.search = {};
    this.getPage(1, 1);
    this.activeFilterId = 0;
    this.activeFilterName = "";
  }
  getPage(page: number, filter = 0) {
    this.currentPage = page;
    this.data = this.loadData().do(res => {
      let ret = res.json();
      if (filter == 1) {
        this.totalTableData = ret.total;
      }
    })
      .map(res => res.json().data);

    return 1;
  }
  loadData() {
    let param: any = {};
    param.limit = this.pageLimit;
    param.page = this.currentPage;
    param.search = this.search;
    param.order = this.order;
    param.user = this.user;
    console.log(param);
    return this.httpService.get("/auth/auth", param);
  }
  selectData(record) {
    let main = this;
    this.selectedRecord = record;
    let title: any = this.translate.get("Notice!");
    let text: any = this.translate.get("Are you sure you want to delete?");
    swal({
      title: title.value,
      text: text.value,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Confirm',
      cancelButtonText: "Cancel"
    }).then(function (isConfirm) {
      if (isConfirm) {
        main.doDelete();
      }
    });
  }
  doEdit(id) {
    this.router.navigate(['synapse/user/user_form/' + id]);
  }
  doView(id) {
    this.router.navigate(['synapse/user/view/' + id]);
  }
  doAdd() {
    this.router.navigate(['synapse/user/user_form']);
  }
  doDelete() {
    this.httpService.delete("/auth/auth/" + this.selectedRecord.id).subscribe((data) => {
      let res = data.json();
      let message = "failed";
      let action = "close";
      if (!res.error) {
        message = "successful";
        this.getPage(this.currentPage, 1);
      }
    });
  }
  getSavedFilter() {
    let param: any = { filter_type: this.currentFunction };
    this.httpService.get("/generic/saved_filter", param).subscribe((data) => {
      let res = data.json();
      this.savedFilter = res.data;
    });
  }
  doSaveFilter() {
    if (this.activeFilterName === "") {
    } else {
      let param: any = { filter_type: this.currentFunction, filters: JSON.stringify(this.search), filter_name: this.activeFilterName, filter_id: this.activeFilterId };
      this.httpService.post("/generic/saved_filter", param).subscribe((data) => {
        this.getSavedFilter();
      });
    }
  }
  doApplyFilter(filters) {
    this.search = JSON.parse(filters.filters);
    this.activeFilterId = filters.id;
    this.activeFilterName = filters.filter_name;
    this.getPage(1, 1);
  }
  doDeleteFilter() {
    this.httpService.delete("/generic/saved_filter/" + this.activeFilterId).subscribe((data) => {
      let res = data.json();
      let message = "failed";
      let action = "close";
      if (!res.error) {
        message = "successful";
        this.getSavedFilter();
      }
    });
  }

}
