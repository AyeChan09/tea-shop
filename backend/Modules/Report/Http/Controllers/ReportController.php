<?php

namespace Modules\Report\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Modules\Expense\Entities\Expense;
use Modules\Slip\Entities\Slip;
use Excel;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('report::index');
    }

    public function getReport(Request $request)
    {
        $input = $request->input();
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $slips = Slip::where(DB::raw('DATE(created_at)'), '>=', $start_date)
                        ->where(DB::raw('DATE(created_at)'), '<=', $end_date)
                        ->select('total', DB::raw('DATE(created_at) as date'))
                        ->get();

        $expenses = Expense::where(DB::raw('DATE(created_at)'), '>=', $start_date)
                        ->where(DB::raw('DATE(created_at)'), '<=', $end_date)
                        ->select('total', DB::raw('DATE(created_at) as date'))
                        ->get();
        
        $indexIncome = 0;
        if(count($slips)>0) {
            $date = $slips[$indexIncome]['date'];
            $income[$indexIncome] = array();
            $income[$indexIncome]['date'] = $date;
            $income[$indexIncome]['income'] = $slips[$indexIncome]['total'];
        } else {
            $income = [];
        }        
        
        for($i=1; $i<count($slips); $i++) {
            if($date == $slips[$i]['date']) {
                $income[$indexIncome]['income'] += $slips[$i]['total'];
            } else {
                $indexIncome++;
                $income[$indexIncome] = array();
                $income[$indexIncome]['date'] = $slips[$i]['date'];
                $income[$indexIncome]['income'] = $slips[$i]['total'];
            }
            $date = $slips[$i]['date'];
        }
        
        $expenses = Expense::where(DB::raw('DATE(created_at)'), '>=', $start_date)
                            ->where(DB::raw('DATE(created_at)'), '<=', $end_date)
                            ->select('total', DB::raw('DATE(created_at) as date'))
                            ->get();
                       
        $indexExpense = 0;
        if(count($expenses)>0) {
            $date = $expenses[$indexExpense]['date'];
            $expense[$indexExpense] = array();
            $expense[$indexExpense]['date'] = $date;
            $expense[$indexExpense]['expense'] = $expenses[$indexExpense]['total'];
        } else {
            $expense = [];
        }
        for($i=1; $i<count($expenses); $i++) {
            if($date == $expenses[$i]['date']) {
                $expense[$indexExpense]['expense'] += $expenses[$i]['total'];
            } else {
                $indexExpense++;
                $expense[$indexExpense] = array();
                $expense[$indexExpense]['date'] = $expenses[$i]['date'];
                $expense[$indexExpense]['expense'] = $expenses[$i]['total'];
            }
            $date = $expenses[$i]['date'];
        }
        
        $index = 0;
        $jStop = count($income);
        $jStart = 0;
        $total_profit = 0;
        $data = [];
        for($i=0; $i<count($expense); $i++) {
            if(count($income)>0) {
                for($j=$jStart; $j<$jStop; $j++) {
                    $data[$index] = array();
                    if($expense[$i]['date'] == $income[$j]['date']) {
                        $data[$index]['date'] = $expense[$i]['date'];
                        $data[$index]['income'] = $income[$j]['income'];
                        $data[$index]['expense'] = $expense[$i]['expense'];
                        $data[$index]['profit'] = $income[$j]['income'] - $expense[$i]['expense'];
                        $j=$jStop;
                        $jStart++;
                    } else if($expense[$i]['date'] > $income[$j]['date']) {
                        $data[$index]['date'] = $income[$j]['date'];
                        $data[$index]['income'] = $income[$j]['income'];
                        $data[$index]['expense'] = 0;
                        $data[$index]['profit'] = $income[$j]['income'];
                        $jStart++;
                    } else {
                        $data[$index]['date'] = $expense[$i]['date'];
                        $data[$index]['income'] = 0;
                        $data[$index]['expense'] = $expense[$i]['expense'];
                        $data[$index]['profit'] = 0 - $expense[$i]['expense'];
                        $j=$jStop;
                    }
                    $total_profit += $data[$index]['profit'];
                    $index++;
                }
            } else {
                $data[$index] = array();
                $data[$index]['date'] = $expense[$i]['date'];
                $data[$index]['income'] = 0;
                $data[$index]['expense'] = $expense[$i]['expense'];
                $data[$index]['profit'] = 0 - $expense[$i]['expense'];
                $total_profit += $data[$index]['profit'];
                $index++;
                $income = [];
            }
        }
        
        $lastData = count($data) - 1;
        if(count($income)>0)
            $lastIncome = count($income) -1;
        else 
            $lastIncome = 0;
                
        if(count($income)>0 && (count($income) > count($data) || $data[$lastData]['date'] < $income[$lastIncome]['date'])) {
            for($j=$jStart; $j<count($income); $j++) {
                $data[$index]['date'] = $income[$j]['date'];
                $data[$index]['income'] = $income[$j]['income'];
                $data[$index]['expense'] = 0;
                $data[$index]['profit'] = $income[$j]['income'];
                $total_profit += $data[$index]['profit'];
                $index++;
            }
        }
        $return['data'] = $data;
        $return['total_profit'] = $total_profit;
        return $return;
    }

    public function getReportDetail(Request $request)
    {
        $input = $request->input();
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $data = DB::table('slip_item')->leftJoin('product', 'slip_item.product_id', 'product.id')
                ->where(DB::raw('DATE(slip_item.created_at)'), '>=', $start_date)
                ->where(DB::raw('DATE(slip_item.created_at)'), '<=', $end_date)
                ->select(DB::raw('DATE(slip_item.created_at) as date'), 'product.name as product', DB::raw('sum(qty) as qty'), 'slip_item.product_id')
                ->groupBy('date','slip_item.product_id')
                ->get();
        
        $return['data'] = $data;
        return $return;
    }

    public function sale_report_excel(Request $request)
    {
        $input = $request->input();
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];

        $return = array();
        $return = $this->getReport($request);
        $file_name = " Sale Report";

        
        Excel::create($file_name, function($excel) use ($return) {
            $excel->sheet('Records', function($sheet) use ($return) {
                $i = 1;
                $j= 'A';
                $sheet->mergeCells('A1:G1');
                $sheet->cell($j.$i, function($cell) {
                    $cell->setValue('Sale Report');
                    $cell->setFontWeight('bold');
                });
                $i++;   //2
                $i++;  //3
                $j = 'A';
                $overall_header = array("Date", "Income", "Expense", "Balance");
                foreach($overall_header as $header) {
                    $sheet->cell($j.$i, function($cell) use ($header) {
                        $cell->setValue($header);
                        $cell->setFontWeight('bold');
                        $cell->setBackground('#c5eff7');
                    });
                    $j++;
                }
                $i++;   //4
                $j='A';
                foreach($return['data'] as $data) {
                    foreach($data as $key=>$value) {
                        $sheet->cell($j.$i, $value);
                        $j++;
                    }
                    $i++;
                    $j='A';
                }
                $i++;
                $j='C';
                $sheet->cell($j.$i, 'Total Profit');
                $j++;
                $sheet->cell($j.$i, $return['total_profit']);
                // foreach ($return['data'] as $record) {
                //     $j = 'A';
                //     $sheet->cell($j.$i, $record['name']);
                //     $j++;
                //     foreach($record['item'] as $item) {
                //         foreach($item as $key=>$value) {
                //             if($key == 'percentage') {
                //                 $sheet->cell($j.$i, $value.'%');
                //             }
                //         }
                //         $j++;                        
                //     }
                //     $sheet->cell($j.$i, $record['total'].'%');
                //     $i++;
                // }
            });
        })->export('xls');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('report::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('report::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('report::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
