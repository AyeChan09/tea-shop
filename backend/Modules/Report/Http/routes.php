<?php

Route::group(['middleware' => [], 'prefix' => 'report', 'namespace' => 'Modules\Report\Http\Controllers'], function()
{
    Route::get('/', 'ReportController@index');
    Route::get('get_report', 'ReportController@getReport');
    Route::get('get_report_detail', 'ReportController@getReportDetail');
    Route::any('/sale_report_excel', 'ReportController@sale_report_excel');
});
