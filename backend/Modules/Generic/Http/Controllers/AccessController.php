<?php

namespace Modules\Generic\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Generic\Entities\MenuItem;
use Illuminate\Support\Facades\DB;

class AccessController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new MenuItem;
    }
    public function get_list_access(Request $request)
    {
        $input = $request->input();
        $user_id = session("user_id") != null ? session("user_id") : 1;
        $route = $input['route'];
        $data = $this->entity->select("acc_admin_menu_item_option.name","access_type","user_access.custom_access")
        ->join("user_access","user_access.acc_admin_menu_id","=","acc_admin_menu_item.id")
        ->join("acc_admin_menu_item_option","acc_admin_menu_item_option.id","=","user_access.acc_admin_menu_option_id")        
        ->where("acc_admin_menu_item.path",$route)->where("user_access.user_id",$user_id);  
         
        $return['data'] = $data->get();
        return $return;
    }
    public function get_list_header(Request $request)
    {
        $input = $request->input();
        $list_name = $input['list_name'];
        if(isset($input['report']) && $input['report'] != ""){
            $data = DB::table("acc_list_setting")->select("label","field_name","type","is_show")        
            ->where("list_name",$list_name)->orderBy("order_id","asc");
            $return['data'] = $data->get();
        }else{
            $data = DB::table("acc_list_setting")->select("label","field_name","type")        
            ->where("list_name",$list_name)->where("is_show",1)->orderBy("order_id","asc");
            $return['data'] = $data->get();
        }
        return $return;
    }
}
