<?php

namespace Modules\Generic\Entities;

use Illuminate\Database\Eloquent\Model;

class menuOption extends Model
{
    protected $fillable = [];
    protected $table = "acc_admin_menu_item_option"; 
}
