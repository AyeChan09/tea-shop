<?php

namespace Modules\Expense\Entities;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [];
    protected $table = "expense"; 

    public function items(){
    	return $this->hasMany("Modules\Expense\Entities\ExpenseItem","expense_id");
    }
}
