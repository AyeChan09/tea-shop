<?php

namespace Modules\Expense\Entities;

use Illuminate\Database\Eloquent\Model;

class CompanyRunningNumber extends Model
{
    protected $fillable = [];
    protected $table = "company_running_number_setting"; 
}
