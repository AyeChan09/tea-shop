<?php

namespace Modules\Expense\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Expense\Entities\Expense;
use Modules\Expense\Entities\ExpenseItem;
use Modules\Expense\Entities\CompanyRunningNumber;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ExpenseController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new Expense;
    }

    public function index(Request $request)
    {
        $input=$request->input();
        $page=$input['page'];
        $limit=$input['limit'];
        $search=json_decode($input['search'],true);
        $order = json_decode($input['order'],true);         
        $data = $this->entity::offset(($page-1)*$limit)->limit($limit)->with('items'); 
        
        if(isset($search['keyword']) && $search['keyword'] != ""){
            $keyword = $search['keyword'];
            $data->where("total",'like',"%".$keyword."%");
        }
        if(isset($order['by']) && $order['by'] != ""){
            $order_dir = $order['dir'] == "" ? "desc" : $order['dir'];
            $data->orderBy($order['by'], $order_dir);
        }         
        $totalData = $data->count("id");
        $data=$data->get();
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('expense::create');
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $itemArray = array();
        $data = new $this->entity;
        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){ 
                $data->$key = $value;
            }
        } 

        //adding companyRunningNubmer
        $number = $data->expense_no;
        if($number == ""){
            $format = CompanyRunningNumber::where("usage","expense")->first();
            $date = Carbon::now();
            $prefix = $format->prefix;
            $formatting = $format->format;
            $last_number = $format->last_number;
            $formatting = str_replace("y",$date->format("y"),$formatting);
            $formatting = str_replace("Y",$date->format("Y"),$formatting);
            $formatting = str_replace("m",$date->format("m"),$formatting);
            $formatting = str_replace("d",$date->format("d"),$formatting);
            $formatting = str_replace("n",$date->format("n"),$formatting);
            $flag = 0;
            while($flag == 0){
                $final_number = str_replace("x",str_pad($last_number++,$format->length,"0",STR_PAD_LEFT),$formatting);
                // $final_number = str_replace("o",str_pad($outlet_id,$format->length,"0",STR_PAD_LEFT), $temp);
                $flag_check = Expense::where("expense_no",$final_number)->first();
                if(!isset($flag_check->id)){
                    $flag = 1;
                }
            }
            $data->expense_no = $final_number;
            $format->last_number = $last_number;
            $return['msg'] = "success_create_quotation ".$data->expense_no;
            $return['error'] = false;                        
        }else{
            $flag_check = Expense::where("expense_no",$data->expense_no)->first();
            if(!isset($flag_check->id)){
                $flag = 1;
            }
            $return['msg'] = "error_duplicate_expense_id";
            $return['error'] = true;            
        }
        // DB::beginTransaction();
        // try {
            $data->save();
            foreach($input['items'] as $expenseItem) {
                $expense_item = new ExpenseItem;
                foreach($expenseItem as $key=>$value){
                    if(Schema::hasColumn($expense_item->getTable(), $key)){ 
                        $expense_item->$key = $value;
                    }
                } 
                $expense_item->expense_id = $data->id;
                $expense_item->save();
            }
            $return['msg']="successful";
            $return['error']=false;
        // } catch(\Exception $e) {
        //     DB::rollback();
        //     $return['error'] = true;
        //     $return['msg'] = 'fail';
        //     $return['exception'] = $e;
        // }
        return $return;  
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $return = array();
        $return['data'] = $this->entity->where("id", $id)->with("items")->first();
        return $return;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('expense::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $this->entity::find($id)->delete();
        ExpenseItem::where('expense_id', $id)->delete();
        $return['error'] = false;
        $return['msg'] = "ok";
        return $return;
    }
}
