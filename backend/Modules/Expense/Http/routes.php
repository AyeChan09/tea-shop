<?php

Route::group(['middleware' => [], 'prefix' => 'expense', 'namespace' => 'Modules\Expense\Http\Controllers'], function()
{
    Route::get('/', 'ExpenseController@index');
    Route::resource('expense', 'ExpenseController');
});
