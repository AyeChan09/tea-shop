<?php

Route::group(['middleware' => [], 'prefix' => 'slip', 'namespace' => 'Modules\Slip\Http\Controllers'], function()
{
    Route::get('/', 'SlipController@index');
    Route::resource('/slip', 'SlipController');
});
