<?php

namespace Modules\Slip\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

use Modules\Slip\Entities\Slip;
use Modules\Slip\Entities\SlipItem;
use Modules\Generic\Entities\UserSavedFilter;

class SlipController extends Controller
{
    private $entity;
    public function __construct() {
        $this->entity = new Slip;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('slip::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('slip::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $items = $input['parameter']['cart'];
        $data = new $this->entity;
        $saveFilter = new UserSavedFilter;

        //saveFilter
        $user_id = session("user_id") != null ? session("user_id") : 1;   
        
        DB::beginTransaction();
        try {
            $data->total = $input['parameter']['subtotal'];
            $data->grand_total = $input['parameter']['grand_total'];
            $data->save();

            foreach($items as $item) {
                $slip_item = new SlipItem;
                foreach($item as $key=>$value) {
                    if(Schema::hasColumn($slip_item->getTable(), $key)) {
                        if($key == 'id') {
                            $slip_item->product_id = $item['id'];
                        } else {
                            $slip_item->$key = $value;
                        }                        
                    }
                    $slip_item->slip_id = $data->id;
                    $slip_item->created_at = $data->created_at;

                    //saveFilter
                    // $saveFilter = UserSavedFilter::where('filter_name', $item['name'])->first();
                    // if(isset($saveFilter->id)) {
                    //     $saveFilter->qty = $saveFilter->qty + 1;
                    // } else {
                    //     $saveFilter['qty'] = $item['qty'];
                    // }
                    // $saveFilter['user_id'] = $user_id;
                    // $saveFilter['filter_type'] = 'product';
                    // $saveFilter['filter_name'] = $item['name'];
                }
                $slip_item->save();
                // $saveFilter->save();
            }            

            DB::commit();
            $return['error'] = false;
            $return['msg'] = 'successful';
        } catch(\Exception $e) {
            DB::rollback();
            $return['error'] = true;
            $return['msg'] = 'fail';
            $return['exception'] = $e;
        }
        return $return;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('slip::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('slip::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
