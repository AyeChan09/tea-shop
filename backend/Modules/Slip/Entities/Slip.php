<?php

namespace Modules\Slip\Entities;

use App\ARmodel as Model;

class Slip extends Model
{
    protected $fillable = [];
    protected $table = "slip";

    public function items() 
    {
        return $this->hasMany('Modules\Slip\Entities\SlipItem', 'slip_id');
    }

}
