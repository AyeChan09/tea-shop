<?php

Route::group(['middleware' => [], 'prefix' => 'product', 'namespace' => 'Modules\Product\Http\Controllers'], function()
{
    // Route::get('/', 'ProductController@index');
    Route::resource('product', 'ProductController');
});
