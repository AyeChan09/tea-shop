<?php

namespace Modules\Auth\Entities;

use App\ARmodel as Model;

class user extends Model
{
    protected $fillable = ['password'];
    protected $table = "user";

    public function session(){
        return $this->hasMany("Modules\Auth\Entities\UserSession");
    }
}
