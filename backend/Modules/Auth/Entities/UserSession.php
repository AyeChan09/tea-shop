<?php

namespace Modules\Auth\Entities;

use App\ARmodel as Model;

class userSession extends Model
{
    protected $fillable = [];
    protected $table = "user_session";
}
