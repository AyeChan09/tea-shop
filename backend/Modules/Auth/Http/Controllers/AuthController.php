<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Validator;

use Modules\Auth\Entities\User;
use Modules\Auth\Entities\UserSession;
use Modules\Auth\Entities\UserPermission;

class AuthController extends Controller
{
    private $entity;
    public function __construct(){
        $this->entity = new User;
    }

    public function index(Request $request)
    {
        $input=$request->input();
        $page=$input['page'];
        $limit=$input['limit'];
        $search=json_decode($input['search'],true);
        $order = json_decode($input['order'],true);  
        $user_id = $input['user'];       
        $data = $this->entity::offset(($page-1)*$limit)->limit($limit)->where('id', '<>', $user_id); 
        
        if(isset($search['keyword']) && $search['keyword'] != ""){
            $keyword = $search['keyword'];
            $data->where("user_no",'like',"%".$keyword."%")
            ->orWhere("name",'like',"%".$keyword."%")
            ->orWhere("email",'like',"%".$keyword."%");
        }
        if(isset($order['by']) && $order['by'] != ""){
            $order_dir = $order['dir'] == "" ? "desc" : $order['dir'];
            $data->orderBy($order['by'], $order_dir);
        }         
        $totalData = $data->count("id");
        $data=$data->get();
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }
  
    public function logout(Request $request){
        $authToken = $request->input("Authorization");
        $authCheck = UserSession::where("token", $authToken)->whereNull("logout_time")->first();
        if(isset($authCheck->id)) {
            $authCheck->logout_time = Carbon::now();
            $authCheck->save();
            $return['status'] = 200;
            $return['error'] = false;
            $return['msg'] = "logout_success";
        } else {
            $return['status'] = 500;
            $return['error'] = true;
            $return['msg'] = "invalid_token";
        }
        return $return;
    }
    
    public function login(Request $request){
        $input = $request->input();
        $username = $input['username'];
        $password = $input['password'];
        $return = array();
        $auth = User::where("username",$username)->first();        
        if(isset($auth->id) && $auth->password == $password){
            $return['error'] = false;
            $return['msg'] = "[[msg_auth_success]]";
            $date = Carbon::now();
            $user_session = new UserSession;
            $user_session->user_id = $auth->id;
            $user_session->created_by = "System";
            $user_session->login_time = $date->toDateTimeString(); 
            $user_session->last_action = $date->toDateTimeString(); 
            $user_session->ip_address = $request->ip();
            $i = true;
            while($i){
                $token = str_random(50);
                $validator = Validator::make(array("token" => $token),array("token"=>"unique:user_session"));
                if($validator->passes()){
                    $user_session->token = $token;
                    $user_session->save();
                    $i = false;
                }
            }
            $user_session->save();
            $return['token'] = $token;
            $return['user_id'] = $auth->id;
            $return['sidebar'] = $this->sideMenu($auth->id);
        }else{ 
            $return['msg'] = "[[msg_auth_failed]]";
            $return['error'] = true;
           
        }
        return $return;
    }
    public function sideMenu($user_id){
        
        $menus = DB::table("acc_admin_menu_item")
                ->leftJoin('user_permission', 'acc_admin_menu_item.id', 'user_permission.acc_admin_menu_item_id')
                ->where("parent_id",0)
                ->where("user_permission.user_id", $user_id)
                ->orderBy("order_id","asc")
                ->get();
        $return = array();
        
        foreach($menus as $menu){
            $temp  = array();
            $temp['path'] = $menu->path == null ? "" : $menu->path;
            $temp['icon'] = $menu->icon;
            $temp['title'] = $menu->title;
            $temp['class'] = $menu->class;
            $temp['badge'] = "";
            $temp['badgeClass'] = "";
            $temp['isExternalLink'] = false;
            
            $submenuArray = array();
            if($menu->hasChild == 1){
                $temp['class'] = "has-sub";
                $submenus = DB::table("acc_admin_menu_item")->where("parent_id",$menu->id)->orderBy("order_id","asc")->get();
                foreach($submenus as $submenu){
                    $temp1 = array();
                    $temp1['path'] = $submenu->path == null ? "" : $submenu->path;
                    $temp1['icon'] = "";
                    $temp1['title'] = $submenu->title;
                    $temp1['class'] = $submenu->class;
                    $temp1['badge'] = "";
                    $temp1['badgeClass'] = "";
                    $temp1['isExternalLink'] = false;
                    $lastmenus = array();
                    if($submenu->hasChild == 1){
                        $sms = DB::table("acc_admin_menu_item")->where("parent_id",$submenu->id)->orderBy("order_id","asc")->get();
                        foreach($sms as $sm){
                            $temp2 = array();
                            $temp2['path'] = $sm->path == null ? "" : $sm->path;
                            $temp2['icon'] = "";
                            $temp2['title'] = $sm->title;
                            $temp2['class'] = $sm->class;
                            $temp2['badge'] = "";
                            $temp2['badgeClass'] = "";
                            $temp2['isExternalLink'] = false;
                            
                            $temp2['submenu'] = array();
                            $lastmenus[] = $temp2;
                        }
                    }
                    $temp1['submenu'] = $lastmenus;
                    
                    $submenuArray[] = $temp1;
                }
            }
            $temp['submenu'] = $submenuArray;
            $return[] = $temp;
        }
        return $return;
    }

    public function sessionCheck(Request $request){
        $authToken = $request->input("Authorization");
        $currentTime = Carbon::now();
        if($authToken !== null){
            $authCheck = UserSession::where("token",$authToken)->whereNull("logout_time")->first();
            if(isset($authCheck->id) && $currentTime->diffInMinutes(Carbon::parse($authCheck->last_action)) <=60){
                $authCheck->last_action = $currentTime;
                $authCheck->save();
                return array("msg"=>"token_valid","status"=>200,"error"=>false);
            }else{
                return array("msg"=>"invalid_token","status"=>500,"error"=>true);
            }
        }else{
            return array("msg"=>"bad_request","status"=>500,"error"=>true,'auth'=>$authToken);            
        }
    }

    public function getOldRecord(Request $request)
    {
        $data = $this->entity->get();
        return $data;
    }

    public function show(Request $request,$id)
    {        
        $return = array();
        $return['data'] = $this->entity->where("id",$id)->first();
        return $return;
    }

    public function change_password(Request $request,$id)
    {
        $return = array();
        $input = $request->input();
        $current_password = $input['current_password'];
        $confirm_password = $input['confirm_password'];

        $data = User::find($id); 
        if($current_password != $data->password) {
            $return['error'] = true;
            $return['message'] = "Your current password is wrong.";
        }else{
            $data->update([
                'password' => $confirm_password
            ]);
            $return['error'] = false;
            $return['message'] = "Change Password.";
        }
       $return['data'] = $data;
        return $return;
    }

    public function store(Request $request)
    {
        $input = $request->input();
        $data = new $this->entity;
        foreach($input as $key=>$value){
            if(Schema::hasColumn($data->getTable(), $key)){ 
                $data->$key = $value;
            }
        } 
        // $data->password = $input['password'];
        $data->save();

        $user_id = $data->id; 
        $permission = new UserPermission;
        $permission->user_id = $user_id;
        $permission->acc_admin_menu_item_id = 3;
        $permission->save();
        
        $return['msg']="successful";
        $return['error']=false;
        $return['data'] = $data;
        $return['permission'] = $permission;
        return $return;  
    }

    public function update(Request $request,$id)
    {        
        $input = $request->input();
        $data = $this->entity->find($id);
        foreach($input as $key=>$value){           
            if(Schema::hasColumn($data->getTable(), $key)){ 
                $data->$key = $value;
            }
        }
        $data->save();
        return $input;
    }

    public function destroy($id)
    {
        $this->entity::find($id)->delete();
        UserPermission::where('user_id', $id)->delete();
        $return['error'] = false;
        $return['msg'] = "ok";
        return $return;
    }
}
