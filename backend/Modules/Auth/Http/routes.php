<?php

Route::group(['middleware' => [], 'prefix' => 'auth', 'namespace' => 'Modules\Auth\Http\Controllers'], function()
{
    Route::resource('auth', 'AuthController');
    Route::post('/register', 'AuthController@register');
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/session_check','AuthController@sessionCheck');
    Route::get('get_old_record','AuthController@getOldRecord');
    Route::post('/change_password/{id}','AuthController@change_password');
});
