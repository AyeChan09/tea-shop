<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Modules\Auth\Entities\UserSession;
use Modules\Auth\Entities\User;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;

class EncryptSend
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(method_exists($response,"getData")){
            $text = $response->getData();
            $response = encrypt(json_encode($text));
        }
        return $response;
    }
}
